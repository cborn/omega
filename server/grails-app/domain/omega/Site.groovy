package omega

class Site {


    String name

    String awsSecretKey

    String awsAccessKey

    String awsBucketRegion

    String awsBucketName

    String moodleUrl

    String moodleKey


    static constraints = {
    }


}
